import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter as Router,Switch,Route } from "react-router-dom";
import registerServiceWorker from './registerServiceWorker';

import Header from './components/shared/Header';
import Dashboard from './components/Dashboard';
import Incomes from './components/Incomes';

ReactDOM.render((
  <div className="container">
    <div id="header">
      <div className="row">
        <div className="col-12">
          <Header/>
        </div>
      </div>
    </div>
    <div id="page">
      <Router>
        <Switch>
          <Route exact path="/dashboard" component={Dashboard}/>
          <Route path="/incomes" component={Incomes}/>
        </Switch>
      </Router>
    </div>
  </div>
), document.getElementById('wrapper'));

registerServiceWorker();
