import React, { Component } from 'react'
import axios from 'axios'

class Incomes extends Component {
  render() {
    return (
      <div>
        <h1> Incomes </h1>
        <table>
         <thead>
           <tr>
               <th>nr.</th>
               <th>Title</th>
               <th>Amount</th>
               <th> Date </th>
           </tr>
         </thead>
         <tbody>
            {this.state.incomes.map((income) => {
              return(
               <tr key={income.id}>
                 <td>{income.id}</td>
                 <td>{income.title}</td>
                 <td>{income.amount}</td>
                 <td>{income.date}</td>
               </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    );
  }

  constructor(props) {
    super(props)
    this.state = {
      incomes: []
    }
  }

  componentDidMount() {
    axios.get('http://localhost:5000/api/v1/incomes.json')
    .then(response => {
      console.log(response)
      this.setState({incomes: response.data})
    })
    .catch(error => console.log(error))
  }
}

export default Incomes
