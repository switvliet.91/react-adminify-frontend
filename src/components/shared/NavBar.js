import React, { Component } from 'react';
import './NavBar.css';

class NavBar extends Component {
  render() {
    return (
      <nav id="main-nav">
        <div className="nav-wrapper">
          <a href="/dashboard" className="brand-logo">Logo</a>
          <ul id="nav-mobile" className="right hide-on-med-and-down">
            <li><a href="/dashboard">Dashboard</a></li>
            <li><a href="/incomes">Incomes</a></li>
            <li><a href="/expenses">Expenses</a></li>
          </ul>
        </div>
      </nav>
    )
  }
}

export default NavBar
